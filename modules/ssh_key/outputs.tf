output "ssh_key" {
  description = "SSH public key"
  value = azurerm_ssh_public_key.ssh_key.public_key
}
