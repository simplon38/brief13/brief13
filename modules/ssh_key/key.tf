resource "azurerm_ssh_public_key" "ssh_key" {
  name                = var.ssh_key_name
  resource_group_name = var.resource_group
  location            = var.location
  public_key          = file("${var.ssh_key_path}")

  tags = {
    client      = var.client
    environment = var.env
    owner       = var.owner
    provisioner = var.provisioner
  }
}
