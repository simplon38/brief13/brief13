//
// Specific to ssh key
//

variable "ssh_key_name" {
  description = "Name of the ssh key"
  type        = string
}

variable "ssh_key_path" {
  description = "Path to the ssh key"
  type        = string
}

//
// Common
//

variable "location" {
  description = "Azure region where the resource groups will be created"
  type        = string
}

variable "resource_group" {
  description = "Name of the resource group"
  type        = string
}

//
// Tags
//

variable "client" {
  description = "Client information"
  type        = string
}

variable "env" {
  description = "Environment information"
  type        = string
}

variable "owner" {
  description = "Owner information"
  type        = string
}

variable "provisioner" {
  description = "Provisioner information"
  type        = string
}
