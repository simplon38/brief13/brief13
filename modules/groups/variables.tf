# Azure Resource Groups
# Creates Azure resource groups based on the provided list of names

//
// Module-specific: Define the list of names for Azure resource groups
//

variable "resource_groups" {
  description = "List of names for Azure resource groups"
  type        = map(string)
}

//
// Tags
//

variable "client" {
  description = "Client tag for resource groups"
  type        = string
  default     = "ACME Corporation"
}

variable "env" {
  description = "Environment tag for resource groups"
  type        = string
  default     = "production"
}

variable "location" {
  description = "Azure region where the resource groups will be created"
  type        = string
  default     = "West Europe"
}

variable "owner" {
  description = "Owner tag for resource groups"
  type        = string
  default     = "John Doe"
}

variable "provisioner" {
  description = "Provisioner tag for resource groups"
  type        = string
  default     = "Terraform"
}
