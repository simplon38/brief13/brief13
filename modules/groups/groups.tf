// https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group

resource "azurerm_resource_group" "main" {
  count    = length(keys(var.resource_groups))
  name     = values(var.resource_groups)[count.index]
  location = var.location

  tags = {
    client      = var.client
    environment = var.env
    owner       = var.owner
    provisioner = var.provisioner
  }
}
