output "resource_group_ids" {
  description = "IDs of the created Azure resource groups"
  value       = { for rg in azurerm_resource_group.main : rg.name => rg.id }
}
