// https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network

# Create a virtual network
resource "azurerm_virtual_network" "main" {
  for_each = var.networks

  name                = each.key
  address_space       = [each.value]
  location            = var.location
  resource_group_name = var.resource_group

  tags = {
    client      = var.client
    environment = var.env
    owner       = var.owner
    provisioner = var.provisioner
  }
}
