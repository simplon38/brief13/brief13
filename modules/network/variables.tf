//
// Specific to network
//

variable "networks" {
  description = "List of names for Azure resource groups"
  type        = map(string)
}

variable "subnet_address" {
  description = "Map of subnet names and prefixes"
  type        = map(string)
}

variable "subnet_name" {
  description = "Map of subnet names and prefixes"
  type        = map(string)
}

variable "subnet_network" {
  description = "Map of subnet names and prefixes"
  type        = map(string)
}

//
// Common
//

variable "location" {
  description = "Azure region where the resource groups will be created"
  type        = string
  default     = "West Europe"
}

variable "resource_group" {
  description = "Name of the resource group"
  type        = string
}

//
// Tags
//

variable "client" {
  description = "Client information"
  type        = string
}

variable "env" {
  description = "Environment information"
  type        = string
}

variable "owner" {
  description = "Owner information"
  type        = string
}

variable "provisioner" {
  description = "Provisioner information"
  type        = string
}
