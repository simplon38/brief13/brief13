output "network" {
  description = "Map of networks names and IDs"
  value       = { for nt in azurerm_virtual_network.main: nt.name => nt.id }
}

output "subnet" {
  description = "Map of subnets names and IDs"
  value       = { 
    for st in azurerm_subnet.main: 
      "${st.name == "AzureBastionSubnet" ? "bastion" : st.name}" => st.id 
  }
}

