# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet

# Create a subnet for the bastion
resource "azurerm_subnet" "main" {
  for_each = var.subnet_name

  name                 = var.subnet_name[each.key]
  resource_group_name  = var.resource_group
  virtual_network_name = azurerm_virtual_network.main[var.subnet_network[each.key]].name
  address_prefixes     = [var.subnet_address[each.key]]
}
