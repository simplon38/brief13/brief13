resource "azurerm_linux_virtual_machine" "brief13" {
  name                = "brief"
  resource_group_name = var.resource_group
  location            = var.location
  size                = "Standard_DS1_v2"
  admin_username      = "brief"
  network_interface_ids = [
    azurerm_network_interface.main.id,
  ]

  admin_ssh_key {
    username   = "brief"
    public_key = var.ssh_key
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "RedHat"
    offer     = "RHEL"
    sku       = "8-LVM"
    version   = "8.8.2023081717"
  }

  computer_name  = "brief"
  disable_password_authentication = true
}


resource "null_resource" "local_exec_example" {
  provisioner "local-exec" {
    command = "./deploy.sh"
    environment = {
      IP = "${azurerm_linux_virtual_machine.brief13.public_ip_address}"
    }
  }
  depends_on = [
    azurerm_linux_virtual_machine.brief13
  ]
}
