# Création d'une adresse IP publique
resource "azurerm_public_ip" "main" {
  name                = "brief-publicip"
  location            = var.location
  resource_group_name = var.resource_group
  allocation_method   = "Static"  # ou "Static" si vous voulez une IP fixe
  sku                 = "Standard"  # ou "Basic" selon vos besoins
}

