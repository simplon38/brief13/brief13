//
// Specific to network
//


variable "subnet_id" {
  description = "Map of subnet names and prefixes"
}


variable "ssh_key" {
  description = "SSH public key"
  type        = string
  
}

//
// Common
//

variable "location" {
  description = "Azure region where the resource groups will be created"
  type        = string
  default     = "West Europe"
}

variable "resource_group" {
  description = "Name of the resource group"
  type        = string
}

//
// Tags
//

variable "client" {
  description = "Client information"
  type        = string
}

variable "env" {
  description = "Environment information"
  type        = string
}

variable "owner" {
  description = "Owner information"
  type        = string
}

variable "provisioner" {
  description = "Provisioner information"
  type        = string
}
