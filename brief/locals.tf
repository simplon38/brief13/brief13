locals {

  // 
  // Module groups
  //
  
  resource_groups = {
    "brief" = "qb-brief-linux"
  }

  //
  // Module SSH Key
  //
  
  ssh_key_name = "admin"
  ssh_key_path = "~/.ssh/id_rsa.pub"

  //
  // Module Network
  //
  
  // Network
  networks = {
    "brief" = "10.30.0.0/16"  
  }

  // Subnet
  subnet_name = {
    "bastion" = "AzureBastionSubnet"
    "subnet1" = "subnet1"
    "subnet2" = "subnet2"
  }

  subnet_address = {
    "bastion" = "10.30.1.0/26"
    "subnet1" = "10.30.2.0/24"
    "subnet2" = "10.30.3.0/24"
  }

  subnet_network = {
    "bastion" = "brief"
    "subnet1" = "brief"
    "subnet2" = "brief"
  }

  env             = "production"
  provisioner     = "Terraform"
  location        = "West Europe"
  client          = "brief"
  owner           = "Quentin Besse"
}
