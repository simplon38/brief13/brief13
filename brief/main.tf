module "groups" {
  source                  = "../modules//groups"

  resource_groups = local.resource_groups

  // Common
  location        = local.location

  // Tags
  env             = local.env
  provisioner     = local.provisioner
  client          = local.client
  owner           = local.owner
}

module "ssh_key" {
  source                  = "../modules//ssh_key"

  ssh_key_name = local.ssh_key_name
  ssh_key_path = local.ssh_key_path

  // Common
  location        = local.location
  resource_group = local.resource_groups.brief

  // Tags
  env             = local.env
  provisioner     = local.provisioner
  client          = local.client
  owner           = local.owner

  depends_on = [
    module.groups
  ]
}

module "network" {
  source                  = "../modules//network"
  
  networks = local.networks
  subnet_address = local.subnet_address
  subnet_name = local.subnet_name
  subnet_network = local.subnet_network

  // Common
  location        = local.location
  resource_group = local.resource_groups.brief

  // Tags
  env             = local.env
  provisioner     = local.provisioner
  client          = local.client
  owner           = local.owner

  depends_on = [
    module.groups
  ]
}

module "vm" {
  source                  = "../modules//vm"

  subnet_id = module.network.subnet["subnet1"]

  ssh_key = module.ssh_key.ssh_key

  location        = local.location
  resource_group = local.resource_groups.brief

  // Tags
  env             = local.env
  provisioner     = local.provisioner
  client          = local.client
  owner           = local.owner
}
